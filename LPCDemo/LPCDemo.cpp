// LPCDemo.cpp : Defines the entry point for the console application.
//

#include <Windows.h>
#include <stdio.h>
#include "LpcDefs.h"

int main()
{
	NTSTATUS ntret = 0;
	OBJECT_ATTRIBUTES oa = {0};
	HMODULE hNtDll = NULL;
	NtCreatePortFn NtCreatePort = NULL;
	NtListenPortFn NtListenPort = NULL;
	NtAcceptConnectPortFn NtAcceptConnectPort = NULL;
	NtCompleteConnectPortFn NtCompleteConnectPort = NULL;
	NtCloseFn NtClose = NULL;
	RtlInitUnicodeStringFn RtlInitUnicodeString = NULL;
	SECURITY_DESCRIPTOR sd = {0};
	HANDLE port = NULL;
	UNICODE_STRING portName = {0};
	BYTE RequestBuffer[sizeof(PORT_MESSAGE) + MAX_LPC_DATA];
	PLPC_MESSAGE lpcMessage = (PLPC_MESSAGE)RequestBuffer;
	HANDLE serverHandle = NULL;
	DWORD validConnectionID = 0x33445566;
	MEMORYSTATUSEX memStatus = { 0 };
	memStatus.dwLength = sizeof(memStatus);

	// Get memory information
	if (!GlobalMemoryStatusEx(&memStatus))
	{
		printf("Failed to get memstat: %u\n", GetLastError());
		goto cleanup;
	}
	printf("Total memory: %I64u %I64u %I64u\n",
		memStatus.ullTotalPageFile,
		memStatus.ullTotalPhys,
		memStatus.ullTotalVirtual
	);

	hNtDll = GetModuleHandleA("ntdll");
	if (!hNtDll)
	{
		printf("Failed to get a handle to ntdll\n");
		goto cleanup;
	}
	RESOLVE(hNtDll, NtCreatePort);
	RESOLVE(hNtDll, NtClose);
	RESOLVE(hNtDll, NtListenPort);
	RESOLVE(hNtDll, RtlInitUnicodeString);
	RESOLVE(hNtDll, NtAcceptConnectPort);
	RESOLVE(hNtDll, NtCompleteConnectPort);

	if (!InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION))
	{
		printf("Failed to init securitydesc\n");
		goto cleanup;
	}

	if (!SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE))
	{
		printf("Failed to init securitydesc\n");
		goto cleanup;
	}
	RtlInitUnicodeString(&portName, L"\\TestPortFOO");
	InitializeObjectAttributes(&oa, &portName, 0, NULL, &sd);

	// Create port
	ntret = NtCreatePort(&port, &oa, 0, sizeof(PORT_MESSAGE) + MAX_LPC_DATA, 0);
	if (STATUS_SUCCESS != ntret)
	{
		printf("Failed to create port: %08x\n", ntret);
		goto cleanup;
	}
	// Port created
	printf("Listening on port...\n");
	ntret = NtListenPort(port, lpcMessage);
	if (STATUS_SUCCESS != ntret)
	{
		printf("Failed to listen on port: %08x\n", ntret);
		goto cleanup;
	}
	printf("Connection received, accepting...\n");
	ntret = NtAcceptConnectPort(&serverHandle,
		NULL,
		(PLPC_MESSAGE)&lpcMessage->Header,
		TRUE,
		NULL,
		NULL);
	if (STATUS_SUCCESS != ntret)
	{
		printf("Failed to accept on port: %08x\n", ntret);
		goto cleanup;
	}

	ntret = NtCompleteConnectPort(serverHandle);
	if (STATUS_SUCCESS != ntret)
	{
		printf("Failed to complete on port: %08x\n", ntret);
		goto cleanup;
	}
	printf("Validating connection: %08x == %08x?\n", lpcMessage->a, validConnectionID);
	if (validConnectionID == lpcMessage->a)
	{
		printf("Valid connection!\n");
	}
	else
	{
		printf("Not a valid connection!\n");
		goto cleanup;
	}
	

cleanup:
	if (serverHandle)
	{
		printf("Closing serverHandle\n");
		NtClose(serverHandle);
	}
	if (port)
	{
		printf("Closing port\n");
		NtClose(port);
	}
	
    return 0;
}

