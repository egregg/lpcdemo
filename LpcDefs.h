#pragma once
#include <Windows.h>
#define NTAPI __stdcall
#define STATUS_SUCCESS  0
#define CSHORT USHORT
#define LPC_CONNECT_FLAG1 0x00000001
#define LPC_CONNECT_FLAG2 0x00000010
#define LPC_CONNECT_FLAG3 0x00000100
#define LPC_CONNECT_FLAG4 0x00001000
#define LPC_CONNECT_FLAG5 0x00010000

#define MAX_LPC_DATA 0x120

typedef struct _UNICODE_STRING {
	USHORT Length;
	USHORT MaximumLength;
	PWSTR  Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _OBJECT_ATTRIBUTES {
	ULONG           Length;
	HANDLE          RootDirectory;
	PUNICODE_STRING ObjectName;
	ULONG           Attributes;
	PVOID           SecurityDescriptor;
	PVOID           SecurityQualityOfService;
}  OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

typedef struct _CLIENT_ID {
	HANDLE UniqueProcess;
	HANDLE UniqueThread;
} CLIENT_ID;

typedef enum _LPC_TYPE
{
	LPC_NEW_MESSAGE,
	LPC_REQUEST,
	LPC_REPLY,
	LPC_DATAGRAM,
	LPC_LOST_REPLY,
	LPC_PORT_CLOSED,
	LPC_CLIENT_DIED,
	LPC_EXCEPTION,
	LPC_DEBUG_EVENT,
	LPC_ERROR_EVENT,
	LPC_CONNECTION_REQUEST,
	LPC_CONNECTION_REFUSED,
	LPC_MAXIMUM
}  LPC_TYPE;

#define LPC_CLIENT_ID CLIENT_ID

typedef struct _PORT_MESSAGE
{
	union {
		struct {
			CSHORT DataLength;
			CSHORT TotalLength;
		} s1;
		ULONG Length;
	} u1;

	union {
		struct {
			CSHORT Type;
			CSHORT DataInfoOffset;
		} s2;
		ULONG ZeroInit;
	} u2;

	union {
		LPC_CLIENT_ID ClientId;
		double DoNotUseThisField;
	};

	ULONG MessageId;

	union {
		ULONGLONG ClientViewSize;
		ULONG CallbackId;
	};
} PORT_MESSAGE, *PPORT_MESSAGE;

typedef struct _LPC_MESSAGE {
	PORT_MESSAGE Header;
	DWORD a;
} LPC_MESSAGE, *PLPC_MESSAGE;

typedef struct _LPC_SECTION_OWNER_MEMORY {
	ULONG                   Length;
	HANDLE                  SectionHandle;
	ULONG                   OffsetInSection;
	ULONG                   ViewSize;
	PVOID                   ViewBase;
	PVOID                   OtherSideViewBase;

} LPC_SECTION_OWNER_MEMORY, *PLPC_SECTION_OWNER_MEMORY;

typedef struct _LPC_SECTION_MEMORY {
	ULONG                   Length;
	ULONG                   ViewSize;
	PVOID                   ViewBase;
} LPC_SECTION_MEMORY, *PLPC_SECTION_MEMORY;

typedef struct _CONNECTION_INFO
{
	DWORD a;
} CONNECTION_INFO, *PCONNECTION_INFO;

#ifndef InitializeObjectAttributes
#define InitializeObjectAttributes( p, n, a, r, s ) {   \
    (p)->Length = sizeof( OBJECT_ATTRIBUTES );          \
    (p)->RootDirectory = r;                             \
    (p)->Attributes = a;                                \
    (p)->ObjectName = n;                                \
    (p)->SecurityDescriptor = s;                        \
    (p)->SecurityQualityOfService = NULL;               \
    }
#endif

typedef NTSTATUS(NTAPI *NtConnectPortFn)(PHANDLE ClientPortHandle,
	PUNICODE_STRING ServerPortName,
	PSECURITY_QUALITY_OF_SERVICE SecurityQos,
	PLPC_SECTION_OWNER_MEMORY ClientSharedMemory,
	PLPC_SECTION_MEMORY ServerSharedMemory,
	PULONG MaximumMessageLength,
	PVOID ConnectionInfo,
	PULONG ConnectionInfoLength);

typedef VOID(WINAPI *RtlInitUnicodeStringFn)(PUNICODE_STRING DestinationString, PCWSTR SourceString);
typedef NTSTATUS(NTAPI *NtCreatePortFn)(PHANDLE Port, POBJECT_ATTRIBUTES oa,
	ULONG MaxConnectInfoLen, ULONG MaxDatLen, PULONG Reserved);
typedef NTSTATUS(NTAPI *NtListenPortFn)(HANDLE Port, PLPC_MESSAGE pConReq);
typedef NTSTATUS(NTAPI *NtCloseFn)(HANDLE ObjectHandle);
typedef NTSTATUS (NTAPI *NtAcceptConnectPortFn) (PHANDLE ServerPortHandle,
	HANDLE AlternativeReceivePortHandle,
	PLPC_MESSAGE ConnectionReply,
	BOOLEAN AcceptConnection,
	PLPC_SECTION_OWNER_MEMORY ServerSharedMemory,
	PLPC_SECTION_MEMORY ClientSharedMemory);
typedef NTSTATUS(NTAPI *NtCompleteConnectPortFn)(HANDLE ServerPortHandle);

#define RESOLVE(h, s) \
	s = (s##Fn)GetProcAddress(hNtDll, #s); \
	if (!s) { printf("Failed to get " #s "\n"); goto cleanup; }