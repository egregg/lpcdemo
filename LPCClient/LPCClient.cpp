// LPCClient.cpp : Defines the entry point for the console application.
//

#include <Windows.h>
#include <stdio.h>
#include "stdafx.h"
#include "LpcDefs.h"

int main()
{
	NTSTATUS ntret = 0;
	OBJECT_ATTRIBUTES oa = { 0 };
	HMODULE hNtDll = NULL;
	NtConnectPortFn NtConnectPort = NULL;
	NtCloseFn NtClose = NULL;
	RtlInitUnicodeStringFn RtlInitUnicodeString = NULL;
	SECURITY_DESCRIPTOR sd = { 0 };
	HANDLE port = NULL;
	UNICODE_STRING portName = { 0 };
	LPC_MESSAGE lpcMessage = { 0 };
	SECURITY_QUALITY_OF_SERVICE sqos = { 0 };
	ULONG maxMsgLen = 0;
	CONNECTION_INFO connectionInfo = { 0 };
	ULONG ciLen = sizeof(connectionInfo);
	MEMORYSTATUSEX memStatus = { 0 };

	memStatus.dwLength = sizeof(memStatus);
	connectionInfo.a = 0x33445566;

	// Get memory information
	if (!GlobalMemoryStatusEx(&memStatus))
	{
		printf("Failed to get memstat: %u\n", GetLastError());
		goto cleanup;
	}
	printf("Total memory: %I64u %I64u %I64u\n",
		memStatus.ullTotalPageFile,
		memStatus.ullTotalPhys,
		memStatus.ullTotalVirtual
	);

	hNtDll = GetModuleHandleA("ntdll");
	if (!hNtDll)
	{
		printf("Failed to get a handle to ntdll\n");
		goto cleanup;
	}
	RESOLVE(hNtDll, NtConnectPort);
	RESOLVE(hNtDll, NtClose);
	RESOLVE(hNtDll, RtlInitUnicodeString);

	RtlInitUnicodeString(&portName, L"\\TestPortFOO");

	// Connect port
	printf("Connecting\n");
	//ntret = NtConnectPort(&port, &portName, &sqos, NULL, NULL, &maxMsgLen, &ci, &ciLen);
	ntret = NtConnectPort(&port, &portName, &sqos, NULL, NULL, &maxMsgLen, &connectionInfo, &ciLen);
	if (STATUS_SUCCESS != ntret)
	{
		printf("Failed to create port: %08x\n", ntret);
		goto cleanup;
	}
	// Port connected
	printf("Connection made!...\n");

cleanup:
	if (port)
	{
		NtClose(port);
	}
	return 0;
}